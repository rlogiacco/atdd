package org.agileware.atdd;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@Cucumber.Options(features = "classpath:./company/advanced-search.feature", glue = "org.agileware.atdd.example.stepdefs", format = "html:target/cucumber", monochrome = true)
public class JUnitWrapper {
}